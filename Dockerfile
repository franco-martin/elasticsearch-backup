FROM registry.hub.docker.com/library/python
COPY requirements.txt /
RUN pip3 install -r /requirements.txt && rm /requirements.txt
ENV PYTHONUNBUFFERED=1
ENV RETENTION_WEEKS=2\
    ELASTICSEARCH_URL=https://localhost:9200/\
    REPOSITORY=your-repo\
    INDICES='.kibana*'\
    USERNAME='svc-account'\
    PASSWORD='very-long-and-complex-password'\
    # Will append '-' and date in the following format 2021-03-06-23.18
    SNAPSHOT_NAME='snapshot'\
    INSTANCE_ID='snapshot-automator'\
    LOGGING_INDEX='snapshots'

#Optional flag to include global state
#ENV INCLUDE_GLOBAL_STATE=1

#Optional flag for backing up a lot of indices. If ASYNC is set, set SNAPSHOT_TIMEOUT_MINS
#ENV ASYNC=false\
#    SNAPSHOT_TIMEOUT_MINS='20'

# Optional flag for skipping TLS verification. Set to anything
ENV SKIP_SSL_VERIFY=true

# Optional variable for running as a loop. Set to how often it should run in seconds
#ENV LOOP=86400
# Optional for disabling snapshot deletion. Set to anything
#ENV DISABLE_SNAPSHOT_DELETION='1'

WORKDIR /app
COPY main.py /app/
CMD ["python3", "main.py"]
