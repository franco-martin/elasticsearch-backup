import requests
import os
from datetime import datetime, timedelta
import logging
from time import sleep

def backup(loop=False):
    logging.info("Creating snapshot")
    # Create the snapshot and wait for it to complete, save the output to output.json
    body = {'indices': os.getenv("INDICES"),
            'ignore_unavailable': True,
            'include_global_state': global_state,
            'metadata': {
                'managed_by': instance_id,
                'delete_by': retention.isoformat(),
                'taken_by': 'Snapshot automator',
                'code': 'https://gitlab.com/franco-martin/elasticsearch-backup',
                'taken_because': 'schedule'
            }
            }
    snap_creation = requests.put("{}/_snapshot/{}/{}/?wait_for_completion={}&pretty".format(elasticsearchurl,
                                                                                    os.getenv("REPOSITORY"),
                                                                                    snapshot_name,
                                                                                    asynchronous),
                            auth=(os.getenv("USERNAME"), os.getenv("PASSWORD")),
                            json=body,
                            verify=ssl)
    logging.debug("Response was: {} - {}".format(snap_creation.status_code, snap_creation.text))
    if os.getenv("ASYNC") is not None:
        logging.info("Looping until snapshot finishes with limit of {} minutes".format(os.getenv("SNAPSHOT_TIMEOUT_MINS")))
        for retries in range(0, int(os.getenv("SNAPSHOT_TIMEOUT_MINS"))):
            snap_status = requests.get(
                "{}/_snapshot/{}/{}".format(elasticsearchurl, os.getenv("REPOSITORY"), snapshot_name),
                auth=(os.getenv("USERNAME"), os.getenv("PASSWORD")),
                verify=ssl
            )
            if snap_status.status_code == 200:
                if snap_status.json()['snapshots'][0]['state'] != 'IN_PROGRESS':
                    if snap_status.json()['snapshots'][0]['state'] == 'SUCCESS':
                        snap_creation = snap_status.json()['snapshots'][0]
                        logging.info("Snapshot created correctly")
                    else:
                        logging.error("Could not create snapshot. Error is {}".format(snap_status.text))
                    break
            else:
                logging.error("Elasticsearch answered with an error status. Error is {}".format(snap_status.text))
                break
            logging.info("Status: {}".format(snap_status.json()['snapshots'][0]['state']))
            sleep(60)
    else:
        if snap_creation.status_code < 299:
            snap_creation = snap_creation.json()['snapshot']
            logging.info("Snapshot created correctly")
        else:
            logging.error(
                "Error logging status, status is: {} \n error is: \n {} -".format(snap_creation.status_code, snap_creation.text))

    if os.getenv("DISABLE_SNAPSHOT_DELETION") is not None:
        logging.info("Snapshot deletion is disabled, exiting...")
        if not loop:
            exit(0)
    # Delete snapshots
    deleted_snapshots = list()
    response = requests.get("{}/_snapshot/{}/*".format(elasticsearchurl, os.getenv("REPOSITORY")),
                            auth=(os.getenv("USERNAME"), os.getenv("PASSWORD")),
                            verify=ssl
                            )
    if response.status_code < 299:
        logging.info("Deleting old snapshots")
        counter = 0
        for snap in response.json()['snapshots']:
            if "metadata" in snap and \
            "managed_by" in snap['metadata'] and \
            "delete_by" in snap['metadata'] and \
            snap['metadata']['managed_by'] == instance_id and \
            datetime.fromisoformat(snap['metadata']['delete_by']) < now:
                print("Deleting '{}'... ".format(snap['snapshot']), end='')
                snap_deletion = requests.delete("{}/_snapshot/{}/{}".format(elasticsearchurl,
                                                                        os.getenv("REPOSITORY"),
                                                                        snap['snapshot']),
                                            auth=(os.getenv("USERNAME"), os.getenv("PASSWORD")),
                                            verify=ssl
                                            )
                if snap_deletion.status_code == 200:
                    counter = counter + 1
                    deleted_snapshots.append(snap['snapshot'])
                    print("OK")
                else:
                    print("ERROR")
                    logging.error("{} - {}".format(response.status_code, snap_deletion.text))
        logging.info("Deleted {} snapshots".format(counter))
    elif response.status_code == 404:
        logging.info("No snapshots to delete")
    else:
        logging.error("Error getting snapshots. Error is {}".format(response.text))

    payload = {
                '@timestamp': "{}".format(date),
                'creation': snap_creation,
                'deletion': deleted_snapshots,
                'instance_id': instance_id
            }
    logging.debug("Logging object: {}".format(payload))
    logging.info("Logging status to elasticsearch")
    # Save data to elasticsearch
    save_status = requests.post(url,
                            json=payload,
                            auth=(os.getenv("USERNAME"), os.getenv("PASSWORD")),
                            verify=ssl)
    if save_status.status_code > 299:
        logging.error(
            "Error logging status, status is: {} \n error is: \n {} -".format(save_status.status_code, save_status.text))


logging.basicConfig(format='%(asctime)s %(message)s')
logging.root.setLevel(level=("INFO" if os.getenv("LOG_LEVEL") is None else os.getenv("LOG_LEVEL")))
#logging.root.setLevel(logging.INFO)
logging.info("Startup")
now = datetime.now()
date = now.isoformat()

#################################### Parameters ###############################################
# Index for logging status
index = os.getenv("LOGGING_INDEX", "snapshots")
# ID of the instance, for running multiple instances
instance_id=os.getenv("INSTANCE_ID","snapshot-automator")
# The elasticsearch endpoint
elasticsearchurl = os.getenv("ELASTICSEARCH_URL")
# The url for logging status
url = "{}/{}/_doc".format(elasticsearchurl, index)
# The name of the snapshot
snapshot_name = "{}-{}".format(os.getenv('SNAPSHOT_NAME'), now.strftime("%Y-%m-%d-%H.%M"))
# Snapshot retention policy. Change '2 month' to whatever
retention = now + timedelta(weeks=int(os.getenv("RETENTION_WEEKS")))
#oldsnapshots = "snapshot-{}".format(retention.strftime("%Y-%m"))

# Username and password are env vars
# This translates into the wait_for_completion parameter. So setting to false means its async.
asynchronous = "false" if os.getenv("ASYNC") is not None else "true"
# Disable SSL Verification if SKIP_SSL_VERIFY is set
ssl = False if os.getenv("SKIP_SSL_VERIFY") is not None else True
if not ssl:
    import urllib3
    urllib3.disable_warnings()
# Include global state
global_state = True if os.getenv("INCLUDE_GLOBAL_STATE") is not None else False
##################################### End of parameters #########################################
print("##################### Elasticsearch Backup #####################")
print("Snapshot Name: {}".format(snapshot_name))
print("Elasticsearch endpoint: {}".format(elasticsearchurl))
print("Logging index: {}".format(index))
print("Repository: {}".format(os.getenv("REPOSITORY")))
print("Retention policy: {} weeks".format(os.getenv("RETENTION_WEEKS")))
print("SSL Verification: {}".format(ssl))
if os.getenv("ASYNC") is not None:
    if os.getenv("SNAPSHOT_TIMEOUT_MINS") is None:
        print("")
        logging.error("ASYNC is set but SNAPSHOT_TIMEOUT_MINS is not. Exiting...")
        exit(0)
    print("Wait for snapshot completion: {}".format(asynchronous))
    print("Snapshot Verification timeout: {}".format(os.getenv('SNAPSHOT_TIMEOUT_MINS')))
print("################################################################")
print("\n")


if os.getenv("LOOP") is None:
    backup()
else:
    print("Running in loop mode every {} seconds ".format(os.getenv("LOOP", default=86400)))
    while True:
        backup(loop=True)
        sleep(int(os.getenv("LOOP", default=86400)))
