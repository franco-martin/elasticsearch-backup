# Purpose
This image creates and deletes snapshots in an existing elasticsearch cluster.

# Retention mechanism
Each snapshot is created with metadata that indicates when the snapshot should be deleted according to the RETENTION_WEEKS variable. After a snapshot is created, all snapshots are read and if they were created by this project (the validation is made by comparing the "managed_by" metadata field) the "delete_by" metadata field is compared to the current date.

# Requirements
- To use this image you will need a working elasticsearch cluster with a working repository.
## Permissions
- Permissions for logging to an index (make sure you restrict only to the index configured in LOGGING_INDEX):
    - indices:data/write/index
    - indices:admin/create
    - indices:data/write/bulk*
    - indices:admin/mapping/put
- Permissions for managing snapshots:
    - manage_snapshots

# Variables
The following are the variables that should be set to configure the application
### RETENTION_WEEKS (2)
For how long should we keep each snapshot. This setting is used in the deletion phase to evaluate using the "delete_by" field if a snapshot should be deleted
### ELASTICSEARCH_URL (https://localhost:9200)
The elasticsearch url including protocol and port.
### REPOSITORY (your-repo)
The repository where we should create the snapshot
### INDICES (.kibana*)
The expression to use to create the indices. Several expressions can be used, for example '.kibana*, metricbeat-*'
### USERNAME (svc-account)
The username that should be used to authenticate with the elasticsearch API
### PASSWORD (very-long-and-complex-password)
The password that should be used to authenticate with the elasticsearch API
### SNAPSHOT_NAME (snapshot)
The first part of the name of the snapshot. We will append '-' and date in the following format 2021-03-06-23.18
If the variable is set to "test" the resulting snapshot will be in the form of "test-2020-03-06-23.18"
### INCLUDE_GLOBAL_STATE
Flag to enable or disable global state in the backup.
### ASYNC
Flag to enable async processing. MUST use with SNAPSHOT_TIMEOUT_MINS.
Strongly adviced to process many indices.
### SNAPSHOT_TIMEOUT_MINS
If the ASYNC flag is activated, sets the timeout for the processing of the snapshot.
### SKIP_SSL_VERIFY
Flag to disable SSL certificate validation.
### DISABLE_SNAPSHOT_DELETION
Flag for disabling snapshot deletion. Not adviced, deletion will me manual.
### LOG_LEVEL
Set the log level to CRITICAL ERROR WARNING INFO DEBUG. Defaults to INFO
### INSTANCE_ID (snapshot-automator)
The instance ID for runnning multiple instances of the software.
### LOGGING_INDEX (snapshots)
The index name for logging to the same cluster.
